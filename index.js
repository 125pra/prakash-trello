let key = "f2178f6a979e993ef492dec52dfa70d3";
let token = "b3d6e6880b5a62873ce570f6ec75d928641af647b8032a709bec87b714074df9";
let boardId = "AFTAp6fB";
let listId = "5d133db760246635df51c2e4";


async function getCardIDs() {

    let url = `https://api.trello.com/1/lists/${listId}/cards?key=${key}&token=${token}`;
    var fetchResult = await fetch(url);
    var response = await fetchResult;
    let data = await response.json();
    await collectCard(data);
    async function collectCard(data) {
        var totalCardsDetail = {};
        await data.forEach(async singleData => {
            var name = singleData["name"];
            totalCardsDetail["name"] = name;
            var id = singleData["id"];
            totalCardsDetail["id"] = id;
            await displayCards(totalCardsDetail);
        });
    }
}

getCardIDs();

async function displayCards(singleData) {
    const listLi = document.querySelector('.container').querySelector('ul');
    var li = document.createElement('li');
    li.className = "cards";
    li.id = singleData["id"]
    li.textContent = singleData["name"];
    listLi.appendChild(li);
    li.className = "cardsName"
    li.setAttribute("class", "cardsName");
}


document.querySelector("#listId").addEventListener("click", async (event) => {
    var cardId = event.target.id
    var nameOfCard = event.target.textContent
    let url = `https://api.trello.com/1/cards/${cardId}/checklists?${key}'&token=${token}`;
    let fetchResult = await fetch(url);
    let response = await fetchResult;
    let data = await response.json();
    await createDivWithAllTheItems(data, nameOfCard, cardId);
})

async function createDivWithAllTheItems(data, nameOfCard, cardId) {
    await data.forEach(async singleData => {
        var checkListId = singleData["id"]
        var checkItems = singleData["checkItems"];
        var parentDiv = document.createElement("div");
        parentDiv.className = "modal";
        parentDiv.id = "myModal";
        var itmesDiv = document.createElement("div");
        itmesDiv.className = "modal-content";
        itmesDiv.id = "itmesId";
        var inputDataForNewItem = document.createElement("input");
        inputDataForNewItem.className = "input";
        inputDataForNewItem.setAttribute("placeholder", "A new Item")
        var addButoon = document.createElement("button");
        addButoon.className = "addButoon";
        addButoon.setAttribute("checkListId", checkListId);
        addButoon.setAttribute("cardId", cardId);
        addButoon.innerHTML = `<class="addItemButton" id="addNewInput" cardId=${cardId} checkListId=${checkListId}>Add Item</>`;
        var closeItmes = document.createElement("span")
        closeItmes.innerHTML = `<span id="close">&times;</span>`;
        itmesDiv.appendChild(closeItmes);
        itmesDiv.appendChild(inputDataForNewItem);
        itmesDiv.appendChild(addButoon)
        var h3 = document.createElement("h3");
        h3.className = "nameOfCard"
        h3.innerHTML = nameOfCard;
        var ul = document.createElement("ul");
        ul.id = "ulId";
        ul.className = "ulClass";
        itmesDiv.appendChild(ul);
        parentDiv.appendChild(itmesDiv)
        parentDiv.appendChild(h3)
        await craeteListOfItems(cardId, h3, ul, checkItems, checkListId, parentDiv, itmesDiv, parentDiv);
        await document.body.appendChild(parentDiv);
        await call();

    })
}

async function craeteListOfItems(cardId, h3, ul, checkItems, checkListId, parentDiv, itmesDiv) {
    checkItems.forEach(async singleData => {
        let nameOfItmes = singleData["name"];
        let itmeid = singleData["id"];
        let state = singleData["state"];
        var li = document.createElement("li")
        li.className = "listeItmes"
        li.setAttribute("id", state);
        li.innerHTML = `
                    <input type="checkbox" id="check" class=${state} ${state === "complete" ? "checked" : null}  card-id=${cardId} checkitem-Id=${itmeid}>
                    <span class=${state} id="itemName">${nameOfItmes}</span>
                    <button class="remove" id="remove" card-id=${cardId} checkitem-Id=${itmeid}>X</button>
                 `
        ul.appendChild(li)

    })
}

async function call() {
    document.getElementById("itmesId").addEventListener("click", async (event) => {
        let targetId = event.target.id
        let target = event.target;

        if (targetId === "addNewInput") {
            await addInput(target);
        }

        if (targetId === "check") {
            await changeStatus(target);
        }

        if (targetId === "remove") {
            await deleteData(target);
        }

        if (targetId === "close") {
         await   close(target);
        }

    })

}


function close(event) {
    var div = document.getElementById('myModal');
    if (div) {
        div.parentNode.removeChild(div);
    }
    var div;
    while (div = document.getElementById('myModal')) {
        div.parentNode.removeChild(div);
    }

}

function deleteData(target) {
    var cardId = target.getAttribute("card-id");
    var itemId = target.getAttribute("checkitem-Id");
    var url = `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?key=${key}&token=${token}` //https://api.trello.com/1/cards/id/checklists/idChecklist
    fetch(url, {
            method: 'DELETE'
        })
        .then(data => data.status === 200 ? target.parentElement.remove() : console.log(data.status));
}



function changeStatus(target) {
    var cardId = target.getAttribute("card-id");
    var itemId = target.getAttribute("checkitem-Id");
    let state = target.checked ? "complete" : "incomplete";
    //https://api.trello.com/1/cards/${cardID}/checkItem/${itemID}?state=${state}&key=${key}&token=${token}

    let url = `https://api.trello.com/1/cards/${cardId}/checkItem/${itemId}?state=${state}&key=${key}&token=${token}`;
    fetch(url, {
            method: 'PUT'
        })
        .then(data => {
            data.status === 200 ? target.parentElement.setAttribute('id', state) : console.log(data)
        });
}



async function addInput(target) {
    let inputdata = document.querySelector('input').value;
    let checkListId = target.getAttribute("checkListId");
    let cardId = target.getAttribute("cardId");

    if (inputdata === "") {
        alert("please enter item");
    } else {
        document.querySelector('input').value = ""
        try {
            let addRequest = await fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${inputdata}&key=${key}&token=${token}`, {
                method: 'POST'
            })
            let item = await addRequest.json();
            var itemid = item.id;
            if (addRequest.status === 200) {
                let state = "incomplete";
                var li = document.createElement("li")
                li.className = "listeItmes"
                li.setAttribute("id", state);
                li.innerHTML = `
                <input type="checkbox" id="check" class=${state} ${state === "complete" ? "checked" : null}  card-id=${cardId} checkitem-Id=${itemid}>
                <span class=${state} id="itemName">${inputdata}</span>
                <button class="remove" id="remove" card-id=${cardId} checkitem-Id=${itemid}>X</button>
             `
                var ul = document.getElementById("ulId")
                ul.appendChild(li)

            }

        } catch (error) {
            console.log(error)
        }

    }

}